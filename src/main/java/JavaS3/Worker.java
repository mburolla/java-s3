package JavaS3;

import com.amazonaws.regions.Regions;
import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

/**
 * A boundry class that wraps S3.
 * @author Martin Burolla
 */
public class Worker {

    //
    // Data members
    //

    private final AmazonS3 s3Client;

    //
    // Constructors
    //

    public Worker() {
        var clientRegion = Regions.US_EAST_1;  // Must have AWS-CLI configured on your local computer.
        this.s3Client = AmazonS3ClientBuilder.standard()
                .withRegion(clientRegion)
                .build();
    }

    //
    // Public Methods
    //

    /**
     * Stores an object in S3.
     */
    public void uploadS3Object(String bucketName, String s3KeyPath, String fileContents) {
        try {
            s3Client.putObject(bucketName, s3KeyPath, fileContents);
            System.out.printf("Uploaded: %s to S3 bucket: %s.%n", s3KeyPath, bucketName);
        }
        catch (SdkClientException e) {
            e.printStackTrace();
        }
    }

    /**
     * Downloads an object from S3.
     */
    public String downloadS3Object (String bucketName, String s3KeyPath) {
        String retval = "";
        try {
            retval = s3Client.getObjectAsString(bucketName, s3KeyPath);
            System.out.printf("Downloaded: %s from S3 bucket: %s.%n", s3KeyPath, bucketName);
        }
        catch (SdkClientException e) {
           e.printStackTrace();
        }
       return retval;
    }

    /**
     * Deletes an object from S3.
     */
    public void deleteObject (String bucketName, String s3KeyPath) {
        try {
            s3Client.deleteObject(bucketName, s3KeyPath);
            System.out.printf("Deleted: %s from S3 bucket: %s.%n", s3KeyPath, bucketName);
        }
        catch (SdkClientException e) {
            e.printStackTrace();
        }
    }
}
