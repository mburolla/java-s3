package JavaS3;

public class Main {
    public static void main(String[] args) {
        var bucketName = "siua-java-s3";
        var s3KeyPath = "marty/today/uploaded_file-1.txt"; // S3 is a flat object store.  These are not real directories.
        var fileContents = "This is the first line.\nThis is the second line.\nThis is the third line.";

        var worker = new Worker();

        // Upload to S3...
        worker.uploadS3Object(bucketName, s3KeyPath, fileContents);

        // Download from S3...
        var objectContents = worker.downloadS3Object(bucketName, s3KeyPath);
        System.out.println(objectContents);

        // Delete from S3...
        //worker.deleteObject(bucketName, s3KeyPath);
    }
}
