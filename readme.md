# Java S3
Simple project that shows how to upload an object to S3 from a String.

# Notes
- S3 is a flat object store even though it appears directories can be created
- The AWS Explorer is a nice plug-in for IntelliJ IDEA that allows you to browse AWS resources such as S3 buckets
- [Amazon Examples](https://docs.aws.amazon.com/sdk-for-java/latest/developer-guide/examples-s3-objects.html)